#!/usr/bin/env python3
# coding: utf-8

from pathlib import Path
import json

from cached_property import cached_property
import m2r

from workman import AbstractExercise, AbstractWorkspace


class Workspace(AbstractWorkspace):
    def _exercises_paths(self, repo_path):
        repo_path = Path(repo_path)
        config_file = repo_path / 'config.json'
        with config_file.open() as fobj:
            all_metadata = json.load(fobj)['exercises']

        for metadata in all_metadata:
            slug = metadata['slug']
            exo_path = repo_path / 'exercises' / slug

            with (exo_path / 'metadata.json').open('w') as fobj:
                json.dump(metadata, fobj)

            yield exo_path


class Exercise(AbstractExercise):
    @cached_property
    def statement(self) -> str:
        # /path_to_exo/README.md'

        # Keep all the content until the section 'Exception messages'
        statement_filepath = self.exercise_data_path / 'README.md'
        with statement_filepath.open() as fobj:
            md_lines = []
            for line in fobj:
                if line.startswith('## Exception messages'):
                    break
                md_lines.append(line)

        return m2r.convert(''.join(md_lines))

    @cached_property
    def example(self) -> str:
        # /path_to_exo/example.py

        example_file_path = self.exercise_data_path / 'example.py'
        with example_file_path.open() as fobj:
            return fobj.read()

    @cached_property
    def unittests(self) -> str:
        # /path_to_exo/exo_name_test.py

        exo_name = self.metadata['slug'].replace('-', '_')
        unittests_file_path = self.exercise_data_path / '{}_test.py'.format(
            exo_name
        )
        with unittests_file_path.open() as fobj:
            # get all the lines but the two last
            # which are "if __name__ == '__main__':\n   unittest.main()"
            content = fobj.readlines()[:-2]
        return ''.join(content)

    @cached_property
    def metadata(self) -> dict:
        # /path_to_exo/metadata.json

        upstream_metadata_path = self.exercise_data_path / 'metadata.json'
        with upstream_metadata_path.open() as fobj:
            upstream_metadata = json.load(fobj)

        metadata = self._default_metadata.copy()
        # we take only the metadata we are interested in
        metadata['unlocked_by'] = upstream_metadata['unlocked_by']
        metadata['slug'] = upstream_metadata['slug']
        metadata['difficulty'] = int(upstream_metadata['difficulty'])
        metadata['topics'] = list(
            set(metadata['topics']) | set(upstream_metadata['topics'] or [])
        )

        # 0 means 'undefined level' for exercism
        # let's assume the exercise is not an easy one.
        if metadata['difficulty'] == 0:
            metadata['difficulty'] = 6

        return metadata

    @cached_property
    def code_base(self) -> str:
        # /path_to_exo/exo_name.py
        code_base_path = self.exercise_data_path / '{}.py'.format(self.name)
        with code_base_path.open() as fobj:
            return fobj.read()

    @cached_property
    def footer(self) -> str:
        return (
            'This exercise is provided by '
            '[Exercism.io](https://exercism.io/) under the MIT license'
        )

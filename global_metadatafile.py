import os
import yaml

data = {}
workspace_path = os.path.join(os.getcwd(), '_build', 'jupyter')
directory = os.path.join(workspace_path, 'exercises')
for dirname in os.listdir(directory):
    metadata_file = os.path.join(directory, dirname, 'metadata.yml')
    if os.path.exists(metadata_file):
        with open(metadata_file) as f:
            data[dirname] = yaml.load(f, Loader=yaml.SafeLoader)
global_file = os.path.join(workspace_path, 'exercises_metadata.yml')
with open(global_file, 'w') as outfile:
    yaml.dump(data, outfile)

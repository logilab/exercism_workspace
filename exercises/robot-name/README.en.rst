
Robot Name
==========

Manage robot factory settings.

When robots come off the factory floor, they have no name.

The first time you boot them up, a random name is generated in the format
of two uppercase letters followed by three digits, such as RX837 or BC811.

Every once in a while we need to reset a robot to its factory settings,
which means that their name gets wiped. The next time you ask, it will
respond with a new random name.

The names must be random: they should not follow a predictable sequence.
Random names means a risk of collisions. Your solution must ensure that
every existing robot has a unique name.

.. literalinclude:: /exercises/robot-name/code_base.py
                    :language: python
                    :class: exercise


.. literalinclude:: /exercises/robot-name/exercise_test.py
                    :language: python
                    :class: test



**Solution**

.. literalinclude:: /exercises/robot-name/example.py
                    :language: python
                    :class: solution

This exercise is provided by [Exercism.io](https://exercism.io/) under the MIT license
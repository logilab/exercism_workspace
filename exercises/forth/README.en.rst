
Forth
=====

Implement an evaluator for a very simple subset of Forth.

`Forth <https://en.wikipedia.org/wiki/Forth_%28programming_language%29>`_
is a stack-based programming language. Implement a very basic evaluator
for a small subset of Forth.

Your evaluator has to support the following words:


* ``+``\ , ``-``\ , ``*``\ , ``/`` (integer arithmetic)
* ``DUP``\ , ``DROP``\ , ``SWAP``\ , ``OVER`` (stack manipulation)

Your evaluator also has to support defining new words using the
customary syntax: ``: word-name definition ;``.

To keep things simple the only data type you need to support is signed
integers of at least 16 bits size.

You should use the following rules for the syntax: a number is a
sequence of one or more (ASCII) digits, a word is a sequence of one or
more letters, digits, symbols or punctuation that is not a number.
(Forth probably uses slightly different rules, but this is close
enough.)

Words are case-insensitive.

.. literalinclude:: /exercises/forth/code_base.py
                    :language: python
                    :class: exercise


.. literalinclude:: /exercises/forth/exercise_test.py
                    :language: python
                    :class: test



**Solution**

.. literalinclude:: /exercises/forth/example.py
                    :language: python
                    :class: solution

This exercise is provided by [Exercism.io](https://exercism.io/) under the MIT license
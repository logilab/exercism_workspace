
Pangram
=======

Determine if a sentence is a pangram. A pangram (Greek: παν γράμμα, pan gramma,
"every letter") is a sentence using every letter of the alphabet at least once.
The best known English pangram is:

..

   The quick brown fox jumps over the lazy dog.


The alphabet used consists of ASCII letters ``a`` to ``z``\ , inclusive, and is case
insensitive. Input will not contain non-ASCII symbols.

.. literalinclude:: /exercises/pangram/code_base.py
                    :language: python
                    :class: exercise


.. literalinclude:: /exercises/pangram/exercise_test.py
                    :language: python
                    :class: test



**Solution**

.. literalinclude:: /exercises/pangram/example.py
                    :language: python
                    :class: solution

This exercise is provided by [Exercism.io](https://exercism.io/) under the MIT license

Two Fer
=======

``Two-fer`` or ``2-fer`` is short for two for one. One for you and one for me.

.. code-block:: text

   "One for X, one for me."

When X is a name or "you".

If the given name is "Alice", the result should be "One for Alice, one for me."
If no name is given, the result should be "One for you, one for me."

.. literalinclude:: /exercises/two-fer/code_base.py
                    :language: python
                    :class: exercise


.. literalinclude:: /exercises/two-fer/exercise_test.py
                    :language: python
                    :class: test



**Solution**

.. literalinclude:: /exercises/two-fer/example.py
                    :language: python
                    :class: solution

This exercise is provided by [Exercism.io](https://exercism.io/) under the MIT license

Word Count
==========

Given a phrase, count the occurrences of each word in that phrase.

For example for the input ``"olly olly in come free"``

.. code-block:: text

   olly: 2
   in: 1
   come: 1
   free: 1

.. literalinclude:: /exercises/word-count/code_base.py
                    :language: python
                    :class: exercise


.. literalinclude:: /exercises/word-count/exercise_test.py
                    :language: python
                    :class: test



**Solution**

.. literalinclude:: /exercises/word-count/example.py
                    :language: python
                    :class: solution

This exercise is provided by [Exercism.io](https://exercism.io/) under the MIT license
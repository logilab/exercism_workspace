
Pascal's Triangle
=================

Compute Pascal's triangle up to a given number of rows.

In Pascal's Triangle each number is computed by adding the numbers to
the right and left of the current position in the previous row.

.. code-block:: text

       1
      1 1
     1 2 1
    1 3 3 1
   1 4 6 4 1
   # ... etc

.. literalinclude:: /exercises/pascals-triangle/code_base.py
                    :language: python
                    :class: exercise


.. literalinclude:: /exercises/pascals-triangle/exercise_test.py
                    :language: python
                    :class: test



**Solution**

.. literalinclude:: /exercises/pascals-triangle/example.py
                    :language: python
                    :class: solution

This exercise is provided by [Exercism.io](https://exercism.io/) under the MIT license

Resistor Color
==============

Resistors have color coded bands, where each color maps to a number. The first 2 bands of a resistor have a simple encoding scheme: each color maps to a single number.

These colors are encoded as follows:


* Black: 0
* Brown: 1
* Red: 2
* Orange: 3
* Yellow: 4
* Green: 5
* Blue: 6
* Violet: 7
* Grey: 8
* White: 9

Mnemonics map the colors to the numbers, that, when stored as an array, happen to map to their index in the array: Better Be Right Or Your Great Big Values Go Wrong.

More information on the color encoding of resistors can be found in the `Electronic color code Wikipedia article <https://en.wikipedia.org/wiki/Electronic_color_code>`_

.. literalinclude:: /exercises/resistor-color/code_base.py
                    :language: python
                    :class: exercise


.. literalinclude:: /exercises/resistor-color/exercise_test.py
                    :language: python
                    :class: test



**Solution**

.. literalinclude:: /exercises/resistor-color/example.py
                    :language: python
                    :class: solution

This exercise is provided by [Exercism.io](https://exercism.io/) under the MIT license
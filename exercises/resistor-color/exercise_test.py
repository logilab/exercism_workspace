# formation_unittest
import unittest



# Tests adapted from `problem-specifications//canonical-data.json` @ v1.0.0

class ResistorColorTest(unittest.TestCase):
    def test_black(self):
        self.assertEqual(color_code('black'), 0)

    def test_white(self):
        self.assertEqual(color_code('white'), 9)

    def test_orange(self):
        self.assertEqual(color_code('orange'), 3)

    def test_colors(self):
        expected = [
            'black',
            'brown',
            'red',
            'orange',
            'yellow',
            'green',
            'blue',
            'violet',
            'grey',
            'white'
        ]
        self.assertEqual(colors(), expected)



# formation widget
from jupyterlab_training import FormUnitTests
FormUnitTests(ResistorColorTest);

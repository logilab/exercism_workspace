
Bob
===

Bob is a lackadaisical teenager. In conversation, his responses are very limited.

Bob answers 'Sure.' if you ask him a question.

He answers 'Whoa, chill out!' if you yell at him.

He answers 'Calm down, I know what I'm doing!' if you yell a question at him.

He says 'Fine. Be that way!' if you address him without actually saying
anything.

He answers 'Whatever.' to anything else.

.. literalinclude:: /exercises/bob/code_base.py
                    :language: python
                    :class: exercise


.. literalinclude:: /exercises/bob/exercise_test.py
                    :language: python
                    :class: test



**Solution**

.. literalinclude:: /exercises/bob/example.py
                    :language: python
                    :class: solution

This exercise is provided by [Exercism.io](https://exercism.io/) under the MIT license

Reverse String
==============

Reverse a string

For example:
input: "cool"
output: "looc"

.. literalinclude:: /exercises/reverse-string/code_base.py
                    :language: python
                    :class: exercise


.. literalinclude:: /exercises/reverse-string/exercise_test.py
                    :language: python
                    :class: test



**Solution**

.. literalinclude:: /exercises/reverse-string/example.py
                    :language: python
                    :class: solution

This exercise is provided by [Exercism.io](https://exercism.io/) under the MIT license
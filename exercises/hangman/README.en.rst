
Hangman
=======

Implement the logic of the hangman game using functional reactive programming.

`Hangman <https://en.wikipedia.org/wiki/Hangman_%28game%29>`_ is a simple word guessing game.

`Functional Reactive Programming <https://en.wikipedia.org/wiki/Functional_reactive_programming>`_ is a way to write interactive
programs. It differs from the usual perspective in that instead of
saying "when the button is pressed increment the counter", you write
"the value of the counter is the sum of the number of times the button
is pressed."

Implement the basic logic behind hangman using functional reactive
programming.  You'll need to install an FRP library for this, this will
be described in the language/track specific files of the exercise.

Hints
-----

Please ignore the part regarding FRP library, a third party library is not required for this exercise.

.. literalinclude:: /exercises/hangman/code_base.py
                    :language: python
                    :class: exercise


.. literalinclude:: /exercises/hangman/exercise_test.py
                    :language: python
                    :class: test



**Solution**

.. literalinclude:: /exercises/hangman/example.py
                    :language: python
                    :class: solution

This exercise is provided by [Exercism.io](https://exercism.io/) under the MIT license
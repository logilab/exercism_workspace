
Saddle Points
=============

Detect saddle points in a matrix.

So say you have a matrix like so:

.. code-block:: text

       1  2  3
     |---------
   1 | 9  8  7
   2 | 5  3  2     <--- saddle point at (2,1)
   3 | 6  6  7

It has a saddle point at (2, 1).

It's called a "saddle point" because it is greater than or equal to
every element in its row and less than or equal to every element in
its column.

A matrix may have zero or more saddle points.

Your code should be able to provide the (possibly empty) list of all the
saddle points for any given matrix.

Note that you may find other definitions of matrix saddle points online,
but the tests for this exercise follow the above unambiguous definition.

.. literalinclude:: /exercises/saddle-points/code_base.py
                    :language: python
                    :class: exercise


.. literalinclude:: /exercises/saddle-points/exercise_test.py
                    :language: python
                    :class: test



**Solution**

.. literalinclude:: /exercises/saddle-points/example.py
                    :language: python
                    :class: solution

This exercise is provided by [Exercism.io](https://exercism.io/) under the MIT license

Parallel Letter Frequency
=========================

Count the frequency of letters in texts using parallel computation.

Parallelism is about doing things in parallel that can also be done
sequentially. A common example is counting the frequency of letters.
Create a function that returns the total frequency of each letter in a
list of texts and that employs parallelism.

.. literalinclude:: /exercises/parallel-letter-frequency/code_base.py
                    :language: python
                    :class: exercise


.. literalinclude:: /exercises/parallel-letter-frequency/exercise_test.py
                    :language: python
                    :class: test



**Solution**

.. literalinclude:: /exercises/parallel-letter-frequency/example.py
                    :language: python
                    :class: solution

This exercise is provided by [Exercism.io](https://exercism.io/) under the MIT license
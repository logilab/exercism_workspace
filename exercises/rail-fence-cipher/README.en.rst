
Rail Fence Cipher
=================

Implement encoding and decoding for the rail fence cipher.

The Rail Fence cipher is a form of transposition cipher that gets its name from
the way in which it's encoded. It was already used by the ancient Greeks.

In the Rail Fence cipher, the message is written downwards on successive "rails"
of an imaginary fence, then moving up when we get to the bottom (like a zig-zag).
Finally the message is then read off in rows.

For example, using three "rails" and the message "WE ARE DISCOVERED FLEE AT ONCE",
the cipherer writes out:

.. code-block:: text

   W . . . E . . . C . . . R . . . L . . . T . . . E
   . E . R . D . S . O . E . E . F . E . A . O . C .
   . . A . . . I . . . V . . . D . . . E . . . N . .

Then reads off:

.. code-block:: text

   WECRLTEERDSOEEFEAOCAIVDEN

To decrypt a message you take the zig-zag shape and fill the ciphertext along the rows.

.. code-block:: text

   ? . . . ? . . . ? . . . ? . . . ? . . . ? . . . ?
   . ? . ? . ? . ? . ? . ? . ? . ? . ? . ? . ? . ? .
   . . ? . . . ? . . . ? . . . ? . . . ? . . . ? . .

The first row has seven spots that can be filled with "WECRLTE".

.. code-block:: text

   W . . . E . . . C . . . R . . . L . . . T . . . E
   . ? . ? . ? . ? . ? . ? . ? . ? . ? . ? . ? . ? .
   . . ? . . . ? . . . ? . . . ? . . . ? . . . ? . .

Now the 2nd row takes "ERDSOEEFEAOC".

.. code-block:: text

   W . . . E . . . C . . . R . . . L . . . T . . . E
   . E . R . D . S . O . E . E . F . E . A . O . C .
   . . ? . . . ? . . . ? . . . ? . . . ? . . . ? . .

Leaving "AIVDEN" for the last row.

.. code-block:: text

   W . . . E . . . C . . . R . . . L . . . T . . . E
   . E . R . D . S . O . E . E . F . E . A . O . C .
   . . A . . . I . . . V . . . D . . . E . . . N . .

If you now read along the zig-zag shape you can read the original message.

.. literalinclude:: /exercises/rail-fence-cipher/code_base.py
                    :language: python
                    :class: exercise


.. literalinclude:: /exercises/rail-fence-cipher/exercise_test.py
                    :language: python
                    :class: test



**Solution**

.. literalinclude:: /exercises/rail-fence-cipher/example.py
                    :language: python
                    :class: solution

This exercise is provided by [Exercism.io](https://exercism.io/) under the MIT license
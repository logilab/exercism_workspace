
Crypto Square
=============

Implement the classic method for composing secret messages called a square code.

Given an English text, output the encoded version of that text.

First, the input is normalized: the spaces and punctuation are removed
from the English text and the message is downcased.

Then, the normalized characters are broken into rows.  These rows can be
regarded as forming a rectangle when printed with intervening newlines.

For example, the sentence

.. code-block:: text

   "If man was meant to stay on the ground, god would have given us roots."

is normalized to:

.. code-block:: text

   "ifmanwasmeanttostayonthegroundgodwouldhavegivenusroots"

The plaintext should be organized in to a rectangle.  The size of the
rectangle (\ ``r x c``\ ) should be decided by the length of the message,
such that ``c >= r`` and ``c - r <= 1``\ , where ``c`` is the number of columns
and ``r`` is the number of rows.

Our normalized text is 54 characters long, dictating a rectangle with
``c = 8`` and ``r = 7``\ :

.. code-block:: text

   "ifmanwas"
   "meanttos"
   "tayonthe"
   "groundgo"
   "dwouldha"
   "vegivenu"
   "sroots  "

The coded message is obtained by reading down the columns going left to
right.

The message above is coded as:

.. code-block:: text

   "imtgdvsfearwermayoogoanouuiontnnlvtwttddesaohghnsseoau"

Output the encoded text in chunks that fill perfect rectangles ``(r X c)``\ ,
with ``c`` chunks of ``r`` length, separated by spaces. For phrases that are
``n`` characters short of the perfect rectangle, pad each of the last ``n``
chunks with a single trailing space.

.. code-block:: text

   "imtgdvs fearwer mayoogo anouuio ntnnlvt wttddes aohghn  sseoau "

Notice that were we to stack these, we could visually decode the
cyphertext back in to the original message:

.. code-block:: text

   "imtgdvs"
   "fearwer"
   "mayoogo"
   "anouuio"
   "ntnnlvt"
   "wttddes"
   "aohghn "
   "sseoau "

.. literalinclude:: /exercises/crypto-square/code_base.py
                    :language: python
                    :class: exercise


.. literalinclude:: /exercises/crypto-square/exercise_test.py
                    :language: python
                    :class: test



**Solution**

.. literalinclude:: /exercises/crypto-square/example.py
                    :language: python
                    :class: solution

This exercise is provided by [Exercism.io](https://exercism.io/) under the MIT license
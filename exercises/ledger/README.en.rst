
Ledger
======

Refactor a ledger printer.

The ledger exercise is a refactoring exercise. There is code that prints a
nicely formatted ledger, given a locale (American or Dutch) and a currency (US
dollar or euro). The code however is rather badly written, though (somewhat
surprisingly) it consistently passes the test suite.

Rewrite this code. Remember that in refactoring the trick is to make small steps
that keep the tests passing. That way you can always quickly go back to a
working version.  Version control tools like git can help here as well.

Please keep a log of what changes you've made and make a comment on the exercise
containing that log, this will help reviewers.

.. literalinclude:: /exercises/ledger/code_base.py
                    :language: python
                    :class: exercise


.. literalinclude:: /exercises/ledger/exercise_test.py
                    :language: python
                    :class: test



**Solution**

.. literalinclude:: /exercises/ledger/example.py
                    :language: python
                    :class: solution

This exercise is provided by [Exercism.io](https://exercism.io/) under the MIT license
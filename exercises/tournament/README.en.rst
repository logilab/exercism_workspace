
Tournament
==========

Tally the results of a small football competition.

Based on an input file containing which team played against which and what the
outcome was, create a file with a table like this:

.. code-block:: text

   Team                           | MP |  W |  D |  L |  P
   Devastating Donkeys            |  3 |  2 |  1 |  0 |  7
   Allegoric Alaskans             |  3 |  2 |  0 |  1 |  6
   Blithering Badgers             |  3 |  1 |  0 |  2 |  3
   Courageous Californians        |  3 |  0 |  1 |  2 |  1

What do those abbreviations mean?


* MP: Matches Played
* W: Matches Won
* D: Matches Drawn (Tied)
* L: Matches Lost
* P: Points

A win earns a team 3 points. A draw earns 1. A loss earns 0.

The outcome should be ordered by points, descending. In case of a tie, teams are ordered alphabetically.

#
-

Input

Your tallying program will receive input that looks like:

.. code-block:: text

   Allegoric Alaskans;Blithering Badgers;win
   Devastating Donkeys;Courageous Californians;draw
   Devastating Donkeys;Allegoric Alaskans;win
   Courageous Californians;Blithering Badgers;loss
   Blithering Badgers;Devastating Donkeys;loss
   Allegoric Alaskans;Courageous Californians;win

The result of the match refers to the first team listed. So this line

.. code-block:: text

   Allegoric Alaskans;Blithering Badgers;win

Means that the Allegoric Alaskans beat the Blithering Badgers.

This line:

.. code-block:: text

   Courageous Californians;Blithering Badgers;loss

Means that the Blithering Badgers beat the Courageous Californians.

And this line:

.. code-block:: text

   Devastating Donkeys;Courageous Californians;draw

Means that the Devastating Donkeys and Courageous Californians tied.

.. literalinclude:: /exercises/tournament/code_base.py
                    :language: python
                    :class: exercise


.. literalinclude:: /exercises/tournament/exercise_test.py
                    :language: python
                    :class: test



**Solution**

.. literalinclude:: /exercises/tournament/example.py
                    :language: python
                    :class: solution

This exercise is provided by [Exercism.io](https://exercism.io/) under the MIT license

High Scores
===========

Manage a game player's High Score list.

Your task is to build a high-score component of the classic Frogger game, one of the highest selling and addictive games of all time, and a classic of the arcade era. Your task is to write methods that return the highest score from the list, the last added score and the three highest scores.

In this exercise you're going to create a **class** and use lists to organize scores.  *Don't worry, it's not as complicated as you think!* 


* `\ **A First Look at Classes** <https://docs.python.org/3/tutorial/classes.html#a-first-look-at-classes>`_ from the Python 3 documentation. 
* `\ **How to Define a Class in Python** <https://realpython.com/python3-object-oriented-programming/#how-to-define-a-class-in-python>`_ from the Real Python website.  
* `\ **Data Structures in Python** <https://docs.python.org/3/tutorial/datastructures.html>`_ from the Python 3 documentation.

.. literalinclude:: /exercises/high-scores/code_base.py
                    :language: python
                    :class: exercise


.. literalinclude:: /exercises/high-scores/exercise_test.py
                    :language: python
                    :class: test



**Solution**

.. literalinclude:: /exercises/high-scores/example.py
                    :language: python
                    :class: solution

This exercise is provided by [Exercism.io](https://exercism.io/) under the MIT license

Transpose
=========

Given an input text output it transposed.

Roughly explained, the transpose of a matrix:

.. code-block:: text

   ABC
   DEF

is given by:

.. code-block:: text

   AD
   BE
   CF

Rows become columns and columns become rows. See https://en.wikipedia.org/wiki/Transpose.

If the input has rows of different lengths, this is to be solved as follows:


* Pad to the left with spaces.
* Don't pad to the right.

Therefore, transposing this matrix:

.. code-block:: text

   ABC
   DE

results in:

.. code-block:: text

   AD
   BE
   C

And transposing:

.. code-block:: text

   AB
   DEF

results in:

.. code-block:: text

   AD
   BE
    F

In general, all characters from the input should also be present in the transposed output.
That means that if a column in the input text contains only spaces on its bottom-most row(s),
the corresponding output row should contain the spaces in its right-most column(s).

.. literalinclude:: /exercises/transpose/code_base.py
                    :language: python
                    :class: exercise


.. literalinclude:: /exercises/transpose/exercise_test.py
                    :language: python
                    :class: test



**Solution**

.. literalinclude:: /exercises/transpose/example.py
                    :language: python
                    :class: solution

This exercise is provided by [Exercism.io](https://exercism.io/) under the MIT license
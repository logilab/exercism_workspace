
Spiral Matrix
=============

Given the size, return a square matrix of numbers in spiral order.

The matrix should be filled with natural numbers, starting from 1
in the top-left corner, increasing in an inward, clockwise spiral order,
like these examples:

Spiral matrix of size 3
#######################

.. code-block:: text

   1 2 3
   8 9 4
   7 6 5

Spiral matrix of size 4
#######################

.. code-block:: text

    1  2  3 4
   12 13 14 5
   11 16 15 6
   10  9  8 7

.. literalinclude:: /exercises/spiral-matrix/code_base.py
                    :language: python
                    :class: exercise


.. literalinclude:: /exercises/spiral-matrix/exercise_test.py
                    :language: python
                    :class: test



**Solution**

.. literalinclude:: /exercises/spiral-matrix/example.py
                    :language: python
                    :class: solution

This exercise is provided by [Exercism.io](https://exercism.io/) under the MIT license
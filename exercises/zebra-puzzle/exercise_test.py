# formation_unittest
import unittest



# Tests adapted from `problem-specifications//canonical-data.json` @ v1.1.0

class ZebraPuzzleTest(unittest.TestCase):
    def test_resident_who_drinks_water(self):
        self.assertEqual(drinks_water(), "Norwegian")

    def test_resident_who_owns_zebra(self):
        self.assertEqual(owns_zebra(), "Japanese")



# formation widget
from jupyterlab_training import FormUnitTests
FormUnitTests(ZebraPuzzleTest);


Zebra Puzzle
============

Solve the zebra puzzle.


#. There are five houses.
#. The Englishman lives in the red house.
#. The Spaniard owns the dog.
#. Coffee is drunk in the green house.
#. The Ukrainian drinks tea.
#. The green house is immediately to the right of the ivory house.
#. The Old Gold smoker owns snails.
#. Kools are smoked in the yellow house.
#. Milk is drunk in the middle house.
#. The Norwegian lives in the first house.
#. The man who smokes Chesterfields lives in the house next to the man with the fox.
#. Kools are smoked in the house next to the house where the horse is kept.
#. The Lucky Strike smoker drinks orange juice.
#. The Japanese smokes Parliaments.
#. The Norwegian lives next to the blue house.

Each of the five houses is painted a different color, and their
inhabitants are of different national extractions, own different pets,
drink different beverages and smoke different brands of cigarettes.

Which of the residents drinks water?
Who owns the zebra?

.. literalinclude:: /exercises/zebra-puzzle/code_base.py
                    :language: python
                    :class: exercise


.. literalinclude:: /exercises/zebra-puzzle/exercise_test.py
                    :language: python
                    :class: test



**Solution**

.. literalinclude:: /exercises/zebra-puzzle/example.py
                    :language: python
                    :class: solution

This exercise is provided by [Exercism.io](https://exercism.io/) under the MIT license
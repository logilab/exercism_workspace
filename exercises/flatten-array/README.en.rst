
Flatten Array
=============

Take a nested list and return a single flattened list with all values except nil/null.

The challenge is to write a function that accepts an arbitrarily-deep nested list-like structure and returns a flattened structure without any nil/null values.

For Example

input: [1,[2,3,null,4],[null],5]

output: [1,2,3,4,5]

.. literalinclude:: /exercises/flatten-array/code_base.py
                    :language: python
                    :class: exercise


.. literalinclude:: /exercises/flatten-array/exercise_test.py
                    :language: python
                    :class: test



**Solution**

.. literalinclude:: /exercises/flatten-array/example.py
                    :language: python
                    :class: solution

This exercise is provided by [Exercism.io](https://exercism.io/) under the MIT license

Isogram
=======

Determine if a word or phrase is an isogram.

An isogram (also known as a "nonpattern word") is a word or phrase without a repeating letter, however spaces and hyphens are allowed to appear multiple times.

Examples of isograms:


* lumberjacks
* background
* downstream
* six-year-old

The word *isograms*\ , however, is not an isogram, because the s repeats.

.. literalinclude:: /exercises/isogram/code_base.py
                    :language: python
                    :class: exercise


.. literalinclude:: /exercises/isogram/exercise_test.py
                    :language: python
                    :class: test



**Solution**

.. literalinclude:: /exercises/isogram/example.py
                    :language: python
                    :class: solution

This exercise is provided by [Exercism.io](https://exercism.io/) under the MIT license
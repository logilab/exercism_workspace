
Nucleotide Count
================

Given a single stranded DNA string, compute how many times each nucleotide occurs in the string.

The genetic language of every living thing on the planet is DNA.
DNA is a large molecule that is built from an extremely long sequence of individual elements called nucleotides.
4 types exist in DNA and these differ only slightly and can be represented as the following symbols: 'A' for adenine, 'C' for cytosine, 'G' for guanine, and 'T' thymine.

Here is an analogy:


* twigs are to birds nests as
* nucleotides are to DNA as
* legos are to lego houses as
* words are to sentences as...

.. literalinclude:: /exercises/nucleotide-count/code_base.py
                    :language: python
                    :class: exercise


.. literalinclude:: /exercises/nucleotide-count/exercise_test.py
                    :language: python
                    :class: test



**Solution**

.. literalinclude:: /exercises/nucleotide-count/example.py
                    :language: python
                    :class: solution

This exercise is provided by [Exercism.io](https://exercism.io/) under the MIT license
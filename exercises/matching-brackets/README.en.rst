
Matching Brackets
=================

Given a string containing brackets ``[]``\ , braces ``{}``\ , parentheses ``()``\ ,
or any combination thereof, verify that any and all pairs are matched
and nested correctly.

.. literalinclude:: /exercises/matching-brackets/code_base.py
                    :language: python
                    :class: exercise


.. literalinclude:: /exercises/matching-brackets/exercise_test.py
                    :language: python
                    :class: test



**Solution**

.. literalinclude:: /exercises/matching-brackets/example.py
                    :language: python
                    :class: solution

This exercise is provided by [Exercism.io](https://exercism.io/) under the MIT license
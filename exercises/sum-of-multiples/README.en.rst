
Sum Of Multiples
================

Given a number, find the sum of all the unique multiples of particular numbers up to
but not including that number.

If we list all the natural numbers below 20 that are multiples of 3 or 5,
we get 3, 5, 6, 9, 10, 12, 15, and 18.

The sum of these multiples is 78.

.. literalinclude:: /exercises/sum-of-multiples/code_base.py
                    :language: python
                    :class: exercise


.. literalinclude:: /exercises/sum-of-multiples/exercise_test.py
                    :language: python
                    :class: test



**Solution**

.. literalinclude:: /exercises/sum-of-multiples/example.py
                    :language: python
                    :class: solution

This exercise is provided by [Exercism.io](https://exercism.io/) under the MIT license
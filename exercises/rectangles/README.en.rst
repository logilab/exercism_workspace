
Rectangles
==========

Count the rectangles in an ASCII diagram like the one below.

.. code-block:: text

      +--+
     ++  |
   +-++--+
   |  |  |
   +--+--+

The above diagram contains 6 rectangles:

.. code-block:: text



   +-----+
   |     |
   +-----+

.. code-block:: text

      +--+
      |  |
      |  |
      |  |
      +--+

.. code-block:: text

      +--+
      |  |
      +--+

.. code-block:: text



      +--+
      |  |
      +--+

.. code-block:: text



   +--+
   |  |
   +--+

.. code-block:: text


     ++
     ++

You may assume that the input is always a proper rectangle (i.e. the length of
every line equals the length of the first line).

.. literalinclude:: /exercises/rectangles/code_base.py
                    :language: python
                    :class: exercise


.. literalinclude:: /exercises/rectangles/exercise_test.py
                    :language: python
                    :class: test



**Solution**

.. literalinclude:: /exercises/rectangles/example.py
                    :language: python
                    :class: solution

This exercise is provided by [Exercism.io](https://exercism.io/) under the MIT license
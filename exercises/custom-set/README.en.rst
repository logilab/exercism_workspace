
Custom Set
==========

Create a custom set type.

Sometimes it is necessary to define a custom data structure of some
type, like a set. In this exercise you will define your own set. How it
works internally doesn't matter, as long as it behaves like a set of
unique elements.

.. literalinclude:: /exercises/custom-set/code_base.py
                    :language: python
                    :class: exercise


.. literalinclude:: /exercises/custom-set/exercise_test.py
                    :language: python
                    :class: test



**Solution**

.. literalinclude:: /exercises/custom-set/example.py
                    :language: python
                    :class: solution

This exercise is provided by [Exercism.io](https://exercism.io/) under the MIT license
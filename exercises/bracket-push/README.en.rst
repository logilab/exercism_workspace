
Bracket Push
============

Given a string containing brackets ``[]``\ , braces ``{}``\ , parentheses ``()``\ ,
or any combination thereof, verify that any and all pairs are matched
and nested correctly.

.. literalinclude:: /exercises/bracket-push/code_base.py
                    :language: python
                    :class: exercise


.. literalinclude:: /exercises/bracket-push/exercise_test.py
                    :language: python
                    :class: test



**Solution**

.. literalinclude:: /exercises/bracket-push/example.py
                    :language: python
                    :class: solution

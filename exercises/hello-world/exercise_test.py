# formation_unittest
import unittest



# Tests adapted from `problem-specifications//canonical-data.json` @ v1.1.0

class HelloWorldTest(unittest.TestCase):
    def test_hello(self):
        self.assertEqual(hello(), 'Hello, World!')



# formation widget
from jupyterlab_training import FormUnitTests
FormUnitTests(HelloWorldTest);

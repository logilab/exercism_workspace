
Hello World
===========

The classical introductory exercise. Just say "Hello, World!".

`"Hello, World!" <http://en.wikipedia.org/wiki/%22Hello,_world!%22_program>`_ is
the traditional first program for beginning programming in a new language
or environment.

The objectives are simple:


* Write a function that returns the string "Hello, World!".
* Run the test suite and make sure that it succeeds.
* Submit your solution and check it at the website.

If everything goes well, you will be ready to fetch your first real exercise.

.. literalinclude:: /exercises/hello-world/code_base.py
                    :language: python
                    :class: exercise


.. literalinclude:: /exercises/hello-world/exercise_test.py
                    :language: python
                    :class: test



**Solution**

.. literalinclude:: /exercises/hello-world/example.py
                    :language: python
                    :class: solution

This exercise is provided by [Exercism.io](https://exercism.io/) under the MIT license
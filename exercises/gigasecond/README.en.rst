
Gigasecond
==========

Calculate the moment when someone has lived for 10^9 seconds.

A gigasecond is 10^9 (1,000,000,000) seconds.

.. literalinclude:: /exercises/gigasecond/code_base.py
                    :language: python
                    :class: exercise


.. literalinclude:: /exercises/gigasecond/exercise_test.py
                    :language: python
                    :class: test



**Solution**

.. literalinclude:: /exercises/gigasecond/example.py
                    :language: python
                    :class: solution

This exercise is provided by [Exercism.io](https://exercism.io/) under the MIT license
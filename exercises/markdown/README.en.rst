
Markdown
========

Refactor a Markdown parser.

The markdown exercise is a refactoring exercise. There is code that parses a
given string with `Markdown
syntax <https://guides.github.com/features/mastering-markdown/>`_ and returns the
associated HTML for that string. Even though this code is confusingly written
and hard to follow, somehow it works and all the tests are passing! Your
challenge is to re-write this code to make it easier to read and maintain
while still making sure that all the tests keep passing.

It would be helpful if you made notes of what you did in your refactoring in
comments so reviewers can see that, but it isn't strictly necessary. The most
important thing is to make the code better!

.. literalinclude:: /exercises/markdown/code_base.py
                    :language: python
                    :class: exercise


.. literalinclude:: /exercises/markdown/exercise_test.py
                    :language: python
                    :class: test



**Solution**

.. literalinclude:: /exercises/markdown/example.py
                    :language: python
                    :class: solution

This exercise is provided by [Exercism.io](https://exercism.io/) under the MIT license
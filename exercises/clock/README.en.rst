
Clock
=====

Implement a clock that handles times without dates.

You should be able to add and subtract minutes to it.

Two clocks that represent the same time should be equal to each other.

.. literalinclude:: /exercises/clock/code_base.py
                    :language: python
                    :class: exercise


.. literalinclude:: /exercises/clock/exercise_test.py
                    :language: python
                    :class: test



**Solution**

.. literalinclude:: /exercises/clock/example.py
                    :language: python
                    :class: solution

This exercise is provided by [Exercism.io](https://exercism.io/) under the MIT license
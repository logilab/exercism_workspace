
Poker
=====

Pick the best hand(s) from a list of poker hands.

See `wikipedia <https://en.wikipedia.org/wiki/List_of_poker_hands>`_ for an
overview of poker hands.

.. literalinclude:: /exercises/poker/code_base.py
                    :language: python
                    :class: exercise


.. literalinclude:: /exercises/poker/exercise_test.py
                    :language: python
                    :class: test



**Solution**

.. literalinclude:: /exercises/poker/example.py
                    :language: python
                    :class: solution

This exercise is provided by [Exercism.io](https://exercism.io/) under the MIT license
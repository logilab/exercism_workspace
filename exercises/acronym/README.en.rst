
Acronym
=======

Convert a phrase to its acronym.

Techies love their TLA (Three Letter Acronyms)!

Help generate some jargon by writing a program that converts a long name
like Portable Network Graphics to its acronym (PNG).

.. literalinclude:: /exercises/acronym/code_base.py
                    :language: python
                    :class: exercise


.. literalinclude:: /exercises/acronym/exercise_test.py
                    :language: python
                    :class: test



**Solution**

.. literalinclude:: /exercises/acronym/example.py
                    :language: python
                    :class: solution

This exercise is provided by [Exercism.io](https://exercism.io/) under the MIT license
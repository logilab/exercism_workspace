
Matrix
======

Given a string representing a matrix of numbers, return the rows and columns of
that matrix.

So given a string with embedded newlines like:

.. code-block:: text

   9 8 7
   5 3 2
   6 6 7

representing this matrix:

.. code-block:: text

       1  2  3
     |---------
   1 | 9  8  7
   2 | 5  3  2
   3 | 6  6  7

your code should be able to spit out:


* A list of the rows, reading each row left-to-right while moving
  top-to-bottom across the rows,
* A list of the columns, reading each column top-to-bottom while moving
  from left-to-right.

The rows for our example matrix:


* 9, 8, 7
* 5, 3, 2
* 6, 6, 7

And its columns:


* 9, 5, 6
* 8, 3, 6
* 7, 2, 7

In this exercise you're going to create a **class**.  *Don't worry, it's not as complicated as you think!* 


* `\ **A First Look at Classes** <https://docs.python.org/3/tutorial/classes.html#a-first-look-at-classes>`_ from the Python 3 documentation. 
* `\ **How to Define a Class in Python** <https://realpython.com/python3-object-oriented-programming/#how-to-define-a-class-in-python>`_ from the Real Python website.  
* `\ **Data Structures in Python** <https://docs.python.org/3/tutorial/datastructures.html>`_ from the Python 3 documentation.

.. literalinclude:: /exercises/matrix/code_base.py
                    :language: python
                    :class: exercise


.. literalinclude:: /exercises/matrix/exercise_test.py
                    :language: python
                    :class: test



**Solution**

.. literalinclude:: /exercises/matrix/example.py
                    :language: python
                    :class: solution

This exercise is provided by [Exercism.io](https://exercism.io/) under the MIT license
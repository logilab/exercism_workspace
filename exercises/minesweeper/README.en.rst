
Minesweeper
===========

Add the numbers to a minesweeper board.

Minesweeper is a popular game where the user has to find the mines using
numeric hints that indicate how many mines are directly adjacent
(horizontally, vertically, diagonally) to a square.

In this exercise you have to create some code that counts the number of
mines adjacent to a square and transforms boards like this (where ``*``
indicates a mine):

.. code-block::

   +-----+
   | * * |
   |  *  |
   |  *  |
   |     |
   +-----+


into this:

.. code-block::

   +-----+
   |1*3*1|
   |13*31|
   | 2*2 |
   | 111 |
   +-----+

.. literalinclude:: /exercises/minesweeper/code_base.py
                    :language: python
                    :class: exercise


.. literalinclude:: /exercises/minesweeper/exercise_test.py
                    :language: python
                    :class: test



**Solution**

.. literalinclude:: /exercises/minesweeper/example.py
                    :language: python
                    :class: solution

This exercise is provided by [Exercism.io](https://exercism.io/) under the MIT license

Anagram
=======

Given a word and a list of possible anagrams, select the correct sublist.

Given ``"listen"`` and a list of candidates like ``"enlists" "google"
"inlets" "banana"`` the program should return a list containing
``"inlets"``.

.. literalinclude:: /exercises/anagram/code_base.py
                    :language: python
                    :class: exercise


.. literalinclude:: /exercises/anagram/exercise_test.py
                    :language: python
                    :class: test



**Solution**

.. literalinclude:: /exercises/anagram/example.py
                    :language: python
                    :class: solution

This exercise is provided by [Exercism.io](https://exercism.io/) under the MIT license
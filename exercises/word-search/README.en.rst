
Word Search
===========

In word search puzzles you get a square of letters and have to find specific
words in them.

For example:

.. code-block:: text

   jefblpepre
   camdcimgtc
   oivokprjsm
   pbwasqroua
   rixilelhrs
   wolcqlirpc
   screeaumgr
   alxhpburyi
   jalaycalmp
   clojurermt

There are several programming languages hidden in the above square.

Words can be hidden in all kinds of directions: left-to-right, right-to-left,
vertical and diagonal.

Given a puzzle and a list of words return the location of the first and last
letter of each word.

.. literalinclude:: /exercises/word-search/code_base.py
                    :language: python
                    :class: exercise


.. literalinclude:: /exercises/word-search/exercise_test.py
                    :language: python
                    :class: test



**Solution**

.. literalinclude:: /exercises/word-search/example.py
                    :language: python
                    :class: solution

This exercise is provided by [Exercism.io](https://exercism.io/) under the MIT license
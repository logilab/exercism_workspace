# formation_unittest
import unittest
import operator



# Tests adapted from `problem-specifications//canonical-data.json` @ v2.4.0

class ListOpsTest(unittest.TestCase):

    # test for append
    def test_append_empty_lists(self):
        self.assertEqual(append([], []), [])

    def test_append_empty_list_to_list(self):
        self.assertEqual(append([], [1, 2, 3, 4]), [1, 2, 3, 4])

    def test_append_nonempty_lists(self):
        self.assertEqual(append([1, 2], [2, 3, 4, 5]),
                         [1, 2, 2, 3, 4, 5])

    # tests for concat
    def test_concat_empty_list(self):
        self.assertEqual(concat([]), [])

    def test_concat_list_of_lists(self):
        self.assertEqual(concat([[1, 2], [3], [], [4, 5, 6]]),
                         [1, 2, 3, 4, 5, 6])

    def test_concat_list_of_nested_lists(self):
        self.assertEqual(
            concat([[[1], [2]], [[3]], [[]], [[4, 5, 6]]]),
            [[1], [2], [3], [], [4, 5, 6]])

    # tests for filter_clone
    def test_filter_empty_list(self):
        self.assertEqual(filter_clone(lambda x: x % 2 == 1, []), [])

    def test_filter_nonempty_list(self):
        self.assertEqual(
            filter_clone(lambda x: x % 2 == 1, [1, 2, 3, 4, 5]),
            [1, 3, 5])

    # tests for length
    def test_length_empty_list(self):
        self.assertEqual(length([]), 0)

    def test_length_nonempty_list(self):
        self.assertEqual(length([1, 2, 3, 4]), 4)

    # tests for map_clone
    def test_map_empty_list(self):
        self.assertEqual(map_clone(lambda x: x + 1, []), [])

    def test_map_nonempty_list(self):
        self.assertEqual(map_clone(lambda x: x + 1, [1, 3, 5, 7]),
                         [2, 4, 6, 8])

    # tests for foldl
    def test_foldl_empty_list(self):
        self.assertEqual(foldl(operator.mul, [], 2), 2)

    def test_foldl_nonempty_list_addition(self):
        self.assertEqual(foldl(operator.add, [1, 2, 3, 4], 5), 15)

    def test_foldl_nonempty_list_floordiv(self):
        self.assertEqual(foldl(operator.floordiv, [2, 5], 5), 0)

    # tests for foldr
    def test_foldr_empty_list(self):
        self.assertEqual(foldr(operator.mul, [], 2), 2)

    def test_foldr_nonempty_list_addition(self):
        self.assertEqual(foldr(operator.add, [1, 2, 3, 4], 5), 15)

    def test_foldr_nonempty_list_floordiv(self):
        self.assertEqual(foldr(operator.floordiv, [2, 5], 5), 2)

    # additional test for foldr
    def test_foldr_add_str(self):
        self.assertEqual(
            foldr(operator.add,
                           ["e", "x", "e", "r", "c", "i", "s", "m"], "!"),
            "exercism!")

    # tests for reverse
    def test_reverse_empty_list(self):
        self.assertEqual(reverse([]), [])

    def test_reverse_nonempty_list(self):
        self.assertEqual(reverse([1, 3, 5, 7]), [7, 5, 3, 1])

    def test_reverse_list_of_lists_not_flattened(self):
        self.assertEqual(reverse([[1, 2], [3], [], [4, 5, 6]]),
                         [[4, 5, 6], [], [3], [1, 2]])

    # additional test for reverse
    def test_reverse_mixed_types(self):
        self.assertEqual(
            reverse(["xyz", 4.0, "cat", 1]), [1, "cat", 4.0, "xyz"])



# formation widget
from jupyterlab_training import FormUnitTests
FormUnitTests(ListOpsTest);

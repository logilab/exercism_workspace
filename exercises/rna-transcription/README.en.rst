
RNA Transcription
=================

Given a DNA strand, return its RNA complement (per RNA transcription).

Both DNA and RNA strands are a sequence of nucleotides.

The four nucleotides found in DNA are adenine (\ **A**\ ), cytosine (\ **C**\ ),
guanine (\ **G**\ ) and thymine (\ **T**\ ).

The four nucleotides found in RNA are adenine (\ **A**\ ), cytosine (\ **C**\ ),
guanine (\ **G**\ ) and uracil (\ **U**\ ).

Given a DNA strand, its transcribed RNA strand is formed by replacing
each nucleotide with its complement:


* ``G`` -> ``C``
* ``C`` -> ``G``
* ``T`` -> ``A``
* ``A`` -> ``U``

.. literalinclude:: /exercises/rna-transcription/code_base.py
                    :language: python
                    :class: exercise


.. literalinclude:: /exercises/rna-transcription/exercise_test.py
                    :language: python
                    :class: test



**Solution**

.. literalinclude:: /exercises/rna-transcription/example.py
                    :language: python
                    :class: solution

This exercise is provided by [Exercism.io](https://exercism.io/) under the MIT license
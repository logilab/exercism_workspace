
Queen Attack
============

Given the position of two queens on a chess board, indicate whether or not they
are positioned so that they can attack each other.

In the game of chess, a queen can attack pieces which are on the same
row, column, or diagonal.

A chessboard can be represented by an 8 by 8 array.

So if you're told the white queen is at (2, 3) and the black queen at
(5, 6), then you'd know you've got a set-up like so:

.. code-block:: text

   _ _ _ _ _ _ _ _
   _ _ _ _ _ _ _ _
   _ _ _ W _ _ _ _
   _ _ _ _ _ _ _ _
   _ _ _ _ _ _ _ _
   _ _ _ _ _ _ B _
   _ _ _ _ _ _ _ _
   _ _ _ _ _ _ _ _

You'd also be able to answer whether the queens can attack each other.
In this case, that answer would be yes, they can, because both pieces
share a diagonal.

.. literalinclude:: /exercises/queen-attack/code_base.py
                    :language: python
                    :class: exercise


.. literalinclude:: /exercises/queen-attack/exercise_test.py
                    :language: python
                    :class: test



**Solution**

.. literalinclude:: /exercises/queen-attack/example.py
                    :language: python
                    :class: solution

This exercise is provided by [Exercism.io](https://exercism.io/) under the MIT license
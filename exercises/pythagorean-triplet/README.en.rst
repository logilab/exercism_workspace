
Pythagorean Triplet
===================

A Pythagorean triplet is a set of three natural numbers, {a, b, c}, for
which,

.. code-block:: text

   a**2 + b**2 = c**2

For example,

.. code-block:: text

   3**2 + 4**2 = 9 + 16 = 25 = 5**2.

There exists exactly one Pythagorean triplet for which a + b + c = 1000.

Find the product a * b * c.

.. literalinclude:: /exercises/pythagorean-triplet/code_base.py
                    :language: python
                    :class: exercise


.. literalinclude:: /exercises/pythagorean-triplet/exercise_test.py
                    :language: python
                    :class: test



**Solution**

.. literalinclude:: /exercises/pythagorean-triplet/example.py
                    :language: python
                    :class: solution

This exercise is provided by [Exercism.io](https://exercism.io/) under the MIT license
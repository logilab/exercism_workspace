
Difference Of Squares
=====================

Find the difference between the square of the sum and the sum of the squares of the first N natural numbers.

The square of the sum of the first ten natural numbers is
(1 + 2 + ... + 10)² = 55² = 3025.

The sum of the squares of the first ten natural numbers is
1² + 2² + ... + 10² = 385.

Hence the difference between the square of the sum of the first
ten natural numbers and the sum of the squares of the first ten
natural numbers is 3025 - 385 = 2640.

.. literalinclude:: /exercises/difference-of-squares/code_base.py
                    :language: python
                    :class: exercise


.. literalinclude:: /exercises/difference-of-squares/exercise_test.py
                    :language: python
                    :class: test



**Solution**

.. literalinclude:: /exercises/difference-of-squares/example.py
                    :language: python
                    :class: solution

This exercise is provided by [Exercism.io](https://exercism.io/) under the MIT license
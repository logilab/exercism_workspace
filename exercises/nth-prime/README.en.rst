
Nth Prime
=========

Given a number n, determine what the nth prime is.

By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that
the 6th prime is 13.

If your language provides methods in the standard library to deal with prime
numbers, pretend they don't exist and implement them yourself.

.. literalinclude:: /exercises/nth-prime/code_base.py
                    :language: python
                    :class: exercise


.. literalinclude:: /exercises/nth-prime/exercise_test.py
                    :language: python
                    :class: test



**Solution**

.. literalinclude:: /exercises/nth-prime/example.py
                    :language: python
                    :class: solution

This exercise is provided by [Exercism.io](https://exercism.io/) under the MIT license
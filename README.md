# exercism Workspace

This workspace is based on the exercises provided by the [exercism
plateform](https://github.com/exercism/python/) under the MIT license.

This repository adapts the exercises from Exercism so they can be used with the
[Jupyterlab-training](https://gitlab.com/logilab/jupyterlab-training) extension.

## Prerequisites

| Package               | Version |
| --------------------- | ------- |
| GitPython             | 2.1.11  |
| PyYAML                | 3.13    |
| Sphinx                | 1.8.3   |
| sphinxcontrib-jupyter | 0.3.0   |
| m2r                   | 0.2.1   |
| workman               | 0.1     |

The Workman package can be found [here](https://gitlab.com/logilab/workman/).


## Updating the repo from upstream

```bash
workspace --update
```

This command will clone the upstream repository, update the exercises and the
metadata in the *exercises/* directory if there is any change from upstream.

## Create the notebooks

```bash
make workspace
```

This command converts all the exercices (.md) to jupyternotebooks, and copies
all static data to the \_build directory (metadata.yml, .py, etc)
